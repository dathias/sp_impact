{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Microsimulation for policy analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## What is microsimulation?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "* Broadly, microsimulation refers to modelling techniques that operate at the level of individual units (micro) to which a set of rules is applied to simulate changes in state or behaviour\n",
    "\n",
    "* These models estimate the outcomes of applying these rules at the micro-level and changes to them, as well as the calculation of any relevant aggregate\n",
    "\n",
    "* Models simulating the effects of social and fiscal policies on household income first developed in the 1980s when the essential inputs – micro-data from household surveys and accessible computing power – became more available"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "notes"
    }
   },
   "source": [
    "In other words, the microsimulation approach simulates the impact of policies at the level at which they are intended to operate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Why microsimulation models?\n",
    "\n",
    "* There are least three key advantages to using microsimulation models when compared to other policy analysis tools:\n",
    "    1. Microsimulations allows for a direct analysis of complex programmatic and behavioural interactions that abound to happen from social policies\n",
    "    2. The approach can be used for both *ex-ante* and *ex-post* policy analysis (future and current policies)\n",
    "    3. The models permit detailed and flexible analyses of the distributional impacts of social policies"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Types of microsimulation models\n",
    "\n",
    "* **Static models** apply purely deterministic policy rules on micro-data. The characteristics of the micro units stay constant\n",
    "\n",
    "* **Dynamic models** “age” the micro units through time, changing their characteristics in response to natural processes and the probabilities of relevant events and transitions\n",
    "\n",
    "* **Behavioural models** use micro-econometric models of individual preferences to estimate the effects of policy changes on behaviour (e.g labour supply)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "notes"
    }
   },
   "source": [
    "In practice the distinction between modelling approaches is no longer necessarily useful as much modern microsimulation analysis combines elements of each type, according to the question being addressed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Key steps in developing a microsimulation with survey data\n",
    "\n",
    "1. Decide on programme parameters:\n",
    "   + Eligibility (based on age, location, wealth status, etc.)\n",
    "   + Benefit level\n",
    "2. Create a binary ‘marker’ to identify eligible people \n",
    "3. Count the number of eligible people in each household\n",
    "4. Calculate value of benefits received by household\n",
    "5. Calculate post-transfer household welfare (exp per capita)\n",
    "6. Calculate poverty statistics using post-transfer welfare variable\n",
    "7. Compare before and after scenarios\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Using R to develop a microsimulation\n",
    "\n",
    "### Goal: simulate the impact of introducing a child benefit programme on poverty"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "1. Decide on programme parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Create objects that reflect on key parameters\n",
    "\n",
    "# maximum age eligibility\n",
    "max_childage <- 17\n",
    "\n",
    "# coverage in percentages\n",
    "coverage <- 100\n",
    "\n",
    "# benefit value in PPP $ per month\n",
    "transfer <- 12"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "2. Create a binary ‘marker’ to identify eligible people"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "#load libraries and dataset\n",
    "library(dplyr)\n",
    "library(srvyr)\n",
    "library(ggplot2)\n",
    "\n",
    "data <- read.csv(\"../data/lic.csv\")\n",
    "\n",
    "# binary markers\n",
    "data <- data %>% mutate(age_elig = (age<=max_childage)*1)\n",
    "\n",
    "# child ranking\n",
    "data <- data %>% arrange(pc_exp_ppp) %>%\n",
    "                 mutate(child_rank = 100*cumsum(weight*age_elig)/sum(weight*age_elig))\n",
    "\n",
    "# child eligible\n",
    "data <- data %>% arrange(pc_exp_ppp) %>%\n",
    "                 mutate(elig = (age_elig==1 & child_rank<=coverage)*1)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "3. Count the number of eligible people in each household"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "data <- data %>% group_by(hhid) %>% add_tally(elig, name = \"n_elig\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "4. Calculate value of benefits received by household"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "data <- data %>% mutate(sp_ben = transfer*n_elig/hhsize)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "5. Calculate post-transfer household welfare (exp per capita)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "data <- data %>% mutate(exp_post = pc_exp_ppp + sp_ben)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "6. Calculate poverty statistics using post-transfer welfare variable"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# define poverty line\n",
    "povline <- 1.9*365/12\n",
    "\n",
    "# create before and after poverty binary variables\n",
    "data <- data %>% mutate(pov = (pc_exp_ppp < povline)*1, pov_post = (exp_post < povline)*1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "7. Compare before and after scenarios\n",
    "\n",
    "* Poverty rates before and after reforms by 5-year age groups"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# label age groups\n",
    "data$age5yrslbl <- factor(data$age5yrs, levels = c(1:16), \n",
    "                       labels = c(\"0-4 years\", \"5-9 years\", \"10-14 years\", \"15-19 years\",\n",
    "                                  \"20-24 years\", \"25-29 years\", \"30-34 years\", \"35-39 years\",\n",
    "                                  \"40-44 years\", \"45-49 years\", \"50-54 years\", \"55-59 years\",\n",
    "                                  \"60-64 years\", \"65-69 years\", \"70-74 years\", \"75+ years\"))\n",
    "\n",
    "# create a survey data object named svy_data\n",
    "svy_data <- data %>% as_survey_design(1, strata = stratum, weight = weight)\n",
    "\n",
    "#create summary statistics\n",
    "stat_table <- svy_data %>% group_by(age5yrslbl) %>% \n",
    "                    summarize(p0 = survey_mean(pov), p0_post = survey_mean(pov_post)) %>% \n",
    "                    mutate(p0 = round(100 * p0, 1), p0_post = round(100*p0_post,1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# summary table\n",
    "stat_table[, c(\"age5yrslbl\", \"p0\", \"p0_post\")]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# graph\n",
    "plotdata <- data.frame(age5years=rep(stat_table$age5yrslbl, times=2),\n",
    "                       poverty = c(stat_table$p0, stat_table$p0_post),\n",
    "                       Pov.Reduction = factor(rep(c(\"Current\", \"Post reforms\"), each = 16), levels = c(\"Current\", \"Post reforms\")))\n",
    "ggplot(plotdata) +\n",
    "            geom_area(aes(x = age5years, y = poverty, group =Pov.Reduction, fill = Pov.Reduction), colour = \"black\", size = .2, alpha = 1, position = \"identity\") +\n",
    "            lims(y = c(0,40)) +\n",
    "            labs(y = \"Poverty headcount (%)\", fill = \"\") +\n",
    "            scale_x_discrete(name = \"Five-year age groups\", labels = c(\"0-4\",\"5-9\",\"10-14\",\"15-19\",\"20-24\",\"25-29\",\"30-34\",\"35-39\",\"40-44\",\"45-49\",\"50-54\",\"55-59\",\"60-64\",\"65-69\",\"70-74\",\"75 +\")) +\n",
    "            scale_color_manual(name = \"\", labels = c(\"Current poverty\"), values = c(\"Current poverty\"= \"black\")) +\n",
    "            scale_fill_manual(values = c(\"#EF5D3B\", \"#203764\") ) +\n",
    "            theme(\n",
    "                plot.background = element_rect(fill = \"white\"),\n",
    "                panel.border = element_blank(),\n",
    "                panel.background = element_rect(fill = \"white\"),\n",
    "                legend.position = 'none',\n",
    "                axis.title.y = element_text(size = 14),\n",
    "                axis.title.x = element_text(size = 14),\n",
    "                axis.text.y = element_text(size = 14),\n",
    "                axis.text.x = element_text(size = 12, angle = 90, hjust = 1),\n",
    "                plot.margin = unit(c(1, 1, 1.85, 1), \"cm\"),\n",
    "                axis.line.x = element_line(colour = \"black\"),\n",
    "                axis.line.y = element_line(colour = \"black\")\n",
    "            )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# https://devpathways.shinyapps.io/sp_sim/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
